const express = require('express');
const app = express();

const mustache = require('mustache-express');



const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const session_config = require('./models/session_config');
const mongoose = require('mongoose');
const config = require('./config');

const usersRouter = require('./routes/users');
const transactionsRouter = require('./routes/transactions');
const accountsRouter = require('./routes/accounts');
const authRouter = require('./routes/auth');
const developerRouter = require(`./routes/developer`);
const apiRouter = require('./routes/api');


const path = require('path');

const viewsDir = path.join(__dirname, 'views');
app.engine('mst', mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
app.use(express.static('public'));


const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(busboyBodyParser({ limit: '15mb', multi: false }));



app.use(cookieParser());
app.use(session({
	secret: config.secret,
	resave: false,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());


const databaseUrl = config.DatabaseUrl;
const serverPort = config.ServerPort;
const connectOptions = {useNewUrlParser: true};



app.get("/", (req, res) => {
	try {
		res.render('index', {user: req.user});
	}
	catch (error) {
		res.sendStatus(500);
	}
});

app.get('/about', (req, res) => {
	try{
		res.render('about', {user: req.user});
	}
	catch(error) {
		res.sendStatus(500);
	}
});

app.get(`/user`, session_config.checkAuth, (req, res) => {
	res.render('user', {
		user: req.user
	});
});

app.use('/users', usersRouter);
app.use('/accounts', accountsRouter);
app.use('/transactions', transactionsRouter);
app.use('/auth', authRouter);
app.use('/developer/v1', developerRouter);
app.use('/api/v1', apiRouter);

mongoose.connect(databaseUrl, connectOptions)
	.then(() => console.log(`Database connected: ${databaseUrl}`))
	.then(() => app.listen(serverPort, () =>  console.log("Server is ready!")))
	.catch((err) => console.log("Start error" + err));