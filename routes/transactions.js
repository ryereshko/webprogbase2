const express = require('express');
const router = express.Router();
const Transaction = require('../models/transactions');
const Account = require(`../models/account`);
const session_config = require('../models/session_config');

router.use(express.static('public'));




router.get('/', session_config.checkAuth, (req, res) => {
	Transaction.getAllFromUser(req.user.id)
		.then(transactions => {
			const page = req.query.page || 0;
			const limit = 5;
			const offset = page * limit;
			if (req.query.str) {
				const category = req.query.str;
				Transaction.getByCategory(category).populate('account').exec()
					.then(transactions => {
						if (transactions) {
							let data = Transaction.pagination(transactions.length, limit, offset, parseInt(page), transactions);
							data.user = req.user;
							res.render('transactions', data);
						}
					})
					.catch(err => {
						console.log(err);
						res.status(500).send("cannot get category in search");
					});
			}
			else {
				if (transactions) {
					let data = Transaction.pagination(transactions.length, limit, offset, parseInt(page), transactions);
					data.user = req.user;
					res.render('transactions', data);
				}
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems...");
		});
});





router.get(`/:index/update`, session_config.checkAuth, (req, res) => {
	Transaction.getById(req.params.index)
		.populate('account')
		.exec()
		.then(transaction => {
			if (typeof transaction === "undefined") res.status(404).send(`There are no transactions with id - ${req.params.index}`);
			else {
				let template = transaction;
				template.count = Math.abs(transaction.count);
				let data = {
					user: req.user,
					transaction: template
				};
				res.render('updateTransaction', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("something wrong in transactions.js line 49");
		});
});
router.post(`/:index/update`, (req, res) => {
	Transaction.getById(req.params.index).populate('account').exec()
		.then(transaction => {
			if (typeof transaction === "undefined") {
				res.status(404).send(`There is no transaction with this id - ${req.params.index}`);
			}
			else {
				const kind = req.body.kind;
				const category = req.body.category;
				const count = parseInt(req.body.count);
				let imgUrl = Transaction.checkCategory(category);
				let transactionNew = new Transaction(transaction.account, kind, category, transaction.account.currency, count, imgUrl, req.user.id);
				let inc = (kind === "fee") ? -count : count;
				let change = inc - transaction.count;
				return Promise.all([Transaction.update(transactionNew, req.params.index), Account.updateCount(transaction.account.id, transactionNew.account.count + change)]);
			}
		})
		.then(() => {
			res.redirect(`/transactions/${req.params.index}`);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Something wrong in transaction.js line 129");
		});
});


router.post(`/:index`, session_config.checkAuth, (req, res) => {
	Transaction.getById(req.params.index)
		.then(transaction => {
			return Transaction.delete(transaction._id);
		})
		.then((transaction) => {
			return Account.removeTransaction(transaction.account, transaction);

		})
		.then(() => {
			res.redirect(`/transactions`);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send(`Cannot delete transactions with id - ${req.params.index}`);
		});
});
router.get('/:index', session_config.checkAuth, (req, res) => {
	let id = req.params.index;
	Transaction.getById(id)
		.populate('account')
		.exec()
		.then(transaction => {
			if (typeof transaction === "undefined") {
				res.status(404).send("cannot find account");
			} else {
				let data = {
					user: req.user,
					transaction: transaction
				};
				res.render('transaction', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems...");
		});
});







module.exports = router;