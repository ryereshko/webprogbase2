const express = require('express');
const router = express.Router();
const config = require('../config.js');
const cloudinary = require('cloudinary');
const path = require('path');

router.use(express.static('public'));


cloudinary.config({
	cloud_name: config.cloudinary.cloud_name,
	api_key: config.cloudinary.api_key,
	api_secret: config.cloudinary.api_secret
});


router.get(`/`, (req, res) => {
	res.sendFile(path.join(__dirname + '/../views/developer.html'));
});




module.exports = router;