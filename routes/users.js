const express = require('express');
const router = express.Router();
const Users = require('../models/users');
const Accounts = require(`../models/account`);
const common = require(`../routes/common`);
const bodyParser = require(`body-parser`);
const busboyBodyParser = require('busboy-body-parser');
const session_config = require('../models/session_config');

router.use(express.static('public'));

router.use(bodyParser.urlencoded({
	extended: true
}));
router.use(bodyParser.json());
router.use(busboyBodyParser({
	limit: '5mb',
	multi: false,
}));


router.get(`/`, session_config.checkAuth, session_config.checkAdmin, (req, res) => {
	Users.getAll()
		.populate('accounts')
		.exec()
		.then(users => {
			const page = req.query.page || 0;
			const limit = 5;
			const offset = page * limit;
			if (users) {
				let data = Users.pagination(users.length, limit, offset, parseInt(page), users);
				data.user = req.user;
				
				res.render('users', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems in line 37 users.js");
		});
});


router.get('/:index', session_config.checkAuth, session_config.checkAdmin, (req, res) => {
	const userId = req.params.index;
	Users.getById(userId)
		.populate('accounts')
		.exec()
		.then(user => {
			if (typeof user === "undefined") {
				res.status(404).send(`No users with this id - ${userId}`);
			} else {

				let data = {
					user: req.user,
					choosenUser: user
				};
				res.render('changeUserRole', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems in line 55 users.js");
		});
});
router.post(`/:index`, session_config.checkAuth, session_config.checkAdmin, (req, res) => {
	Users.getById(req.params.index)
		.then(choosenUser => {
			if (!choosenUser) {
				res.status(404).send(`No users with this id - ${req.params.index}`);
			}
			else {
				const role = req.body.role;
				return Users.updateRole(req.params.index, role);
			}
		})
		.then(() => {
			res.redirect('/users');
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems in line 84 users.js");
		});
});
router.get(`/:index/addAccount`, session_config.checkAuth, (req, res) => {
	Users.getById(req.params.index)
		.populate('accounts')
		.exec()
		.then(user => {
			if (typeof user === "undefined") res.status(404).send(`There are no users with id - ${req.params.index}`);
			else {
				res.render('newacc', user);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("something wrong in users.js line 103");
		});
});
router.post(`/:index/addAccount`, session_config.checkAuth, (req, res) => { // page with forms to create new account
	const file = req.files.photoFile;
	if (file) {
		Users.getById(req.params.index).populate('accounts').exec()
			.then(user => {
				common.savePic(file)
					.then(photo => {
						const name = req.body.name;
						const count = req.body.count;
						const currency = req.body.currency;
						let accountNew = new Accounts(name, count, currency, user._id, photo.url);
						return Accounts.insert(accountNew);
					})
					.then(account => {
						return Users.addAccount(account);
					})
					.then(() => {
						res.redirect(`/users/${req.params.index}`);
					})
					.catch(err => {
						console.log(err);
						res.status(500).send(`Cannot add account to user with id - ${req.params.index}`);
					});
			})
			.catch(err => {
				console.log(err);
				res.status(500).send(`Cannot get user with id - ${req.params.index}`);
			});
	}
	else {
		res.redirect(`/users/${req.params.index}/addAccount`);
	}
});







module.exports = router;