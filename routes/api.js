const express = require('express');
const config = require('../config.js');
const session_config = require('../models/session_config');
const common = require('../routes/common');
const cloudinary = require('cloudinary');
const passport = require('../models/passport_config');
const router = express.Router();

const User = require(`../models/users`);
const Transaction = require(`../models/transactions`);
const Account = require(`../models/account`);


router.use(express.static('public'));


cloudinary.config({
	cloud_name: config.cloudinary.cloud_name,
	api_key: config.cloudinary.api_key,
	api_secret: config.cloudinary.api_secret
});


router.get(`/`, (req, res) => {
	res.json(JSON.stringify());
});
router.get(`/me`, passport.authenticate('basic', { session: false }), (req, res) => {
	let user = req.user;
	user.password = "undefined";
	res.json(user);
});



// -------------------- users -------------------------------------

router.post(`/users/new`, async (req, res) => {
	const username = req.body.username;
	let user = await User.getByLogin(username);
	if (!user) {
		const password = req.body.password;
		const confirm = req.body.confirm;
		if (password === confirm) {
			const fullname = req.body.fullname;
			let photo = await common.savePic(req.files.image);
			if (photo) {
				let newUser = new User(username, session_config.sha512(password, session_config.serverSalt).passwordHash, fullname, photo.url);
				User.insert(newUser)
					.then(newUser => {
						newUser.password = "undefind";
						res.status(201).json(newUser);
					})
					.catch(err => {
						res.status(409).send(err.message);
					});
			}
			else {
				res.status(400).send("Please, upload some photo");
			}
		}
		else {
			res.status(400).send("Passwords aren`t identical");;
		}
	}
	else {
		res.status(400).send(`This ${username} login alredy exists`);
	}
});


router.get(`/users`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	if (req.user.role === 1) {
		User.getAll()
			.populate('accounts')
			.exec()
			.then(users => {
				const page = req.query.page || 0;
				const limit = 2;
				const offset = page * limit;
				if (users) {
					let data = User.pagination(users.length, limit, offset, parseInt(page), users);
					for (let user of data.users) {
						user.password = "undefined";
					}
					res.status(200).json(data.users);
				}
			})
			.catch(err => {
				console.log(err);
				res.status(500).send("Cannot load users");
			});
	}
	else {
		res.sendStatus(403);
	}
});

router.get(`/users/:index`, passport.authenticate('basic', { session: false }), (req, res) => {
	if (req.user.role === 1) {
		User.getById(req.params.index).populate('accounts').exec()
			.then(user => {
				if (typeof user === "undefined") {
					res.status(404).send(`No users with this id - ${req.params.index}`);
				} else {
					user.password = "undefined";
					res.status(200).json(user);
				}
			})
			.catch();
	}
	else {
		res.sendStatus(403);
	}
});

router.patch(`/users/:index/update`, passport.authenticate(`basic`, { session: false }), async (req, res) => {
	if (req.user.role === 1) {
		let user = await User.getById(req.params.index);
		if (typeof user !== "indefined") {
			let updatedUser = await User.getByLogin(req.body.username);
			if (!updatedUser) {
				const username = req.body.username;
				const password = req.body.password;
				const confirm = req.body.confirm;
				if (password === confirm) {
					const fullname = req.body.fullname;
					let photo = await common.savePic(req.files.image);
					if (photo) {
						let newUser = new User(username, session_config.sha512(password, session_config.serverSalt).passwordHash, fullname, photo.url);
						User.insert(newUser)
							.then(newUser => {
								newUser.password = "undefind";
								res.status(201).json(newUser);
							})
							.catch(err => {
								res.status(409).send(err.message);
							});
					}
					else {
						res.status(400).send("Please, upload some photo");
					}
				}
				else {
					res.status(400).send("Passwords aren`t identical");;
				}
			}
			else {
				res.status(400).send(`This ${req.body.username} login alredy exists`);
			}
		}
		else {
			res.status(404).send(`There are no users with id - ${req.params.index}`);
		}
	}
	else {
		res.sendStatus(403);
	}

});

router.delete(`/users/:index`, passport.authenticate(`basic`, { session: false }), async (req, res) => {
	if (req.user.role === 1) {
		let user = await User.getById(req.params.index).populate('accounts').exec();
		if (user) {
			let transactions = await Transaction.getAllFromUser(user._id).populate('owner').exec();
			for (let transaction of transactions) {
				await Transaction.delete(transaction._id);
			}
			for (let account of user.accounts) {
				await Transaction.deleteFromAccount(account._id);
				await Account.delete(account._id);
			}
			await Account.deleteFromUser(user._id);
			User.delete(user)
				.then(() => {
					res.sendStatus(200);
				})
				.catch(err => {
					res.status(500).send(err.message);

				});
		}
		else {
			res.status(404).send(`There are no users with id - ${req.params.index}`);
		}
	}
	else {
		res.sendStatus(403);
	}

});



// --------------- Accounts -------------------------------
router.post(`/accounts/new`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	User.getByLogin(req.user.username)
		.then(user => {
			const photo = req.files.image;
			return Promise.all([common.savePic(photo), user]);

		})
		.then(([photo, user]) => {
			const name = req.body.name;
			const count = parseInt(req.body.count);
			const currency = req.body.currency;
			let account = new Account(name, count, currency, user._id, photo.url);
			return Account.insert(account);
		})
		.then((account) => {
			return User.addAccount(account);
		})
		.then((account) => {
			res.status(201).json(account);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Cannot add new account");
		});
});

router.get(`/accounts`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	User.getById(req.user.id)
		.then(user => {
			if (req.query.str) {
				const name = req.query.str;
				return Account.getByName(name, user.id);
			}
			else {
				return Account.getAllFromUser(user.id);
			}
		})
		.then(accounts => {
			const page = req.query.page || 0;
			const limit = 3;
			const offset = page * limit;
			if (accounts.length > 0) {
				let accountAray = Account.pagination(accounts.length, limit, offset, parseInt(page), accounts).accounts;
				res.status(200).json(accountAray);
			}
			else {
				res.json({});
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Cannot upload accounts");
		});
});


router.get(`/accounts/:index`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	User.getById(req.user.id)
		.then(user => {
			return Account.getByIdFromUser(req.params.index, user._id);
		})
		.then(account => {
			if (typeof account !== "undefined") {
				res.status(200).json(account);
			}
			else {
				res.status(404).json({
					message: `There is no account with this if - ${req.params.index}`
				});
			}
		})
		.catch(err => {
			res.status(500).send(err.message);
		});
});


router.patch(`/accounts/:index/update`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	Account.getById(req.params.index)
		.then(account => {
			if (typeof account === "undefined") {
				res.status(404).json({
					message: `There is no account with this if - ${req.params.index}` 
				});
				return Promise.reject();
			}
			else {
				const files = req.files.image;
				return Promise.all([common.savePic(files), account]);
			}
		})
		.then(([photo, account]) => {
			const name = (!req.body.name === null) ? account.name : req.body.name;
			const count = (!req.body.count) ? account.count : parseInt(req.body.count);
			const currency = (!req.body.currency === null) ? account.currency : req.body.currency;
			let accountNew = new Account(name, count, currency, account.owner, photo.url);
			return Promise.all([Account.update(accountNew, req.params.index), Account.getById(account._id)]);
		})
		.then(([oldAccount, newAccount]) => {
			res.status(200).json(newAccount);
		})
		.catch(err => {
			res.status(500).send(err.message);
		});
});

router.delete(`/accounts/:index`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	Promise.all([Transaction.deleteFromAccount(req.params.index), Account.delete(req.params.index), User.removeAccount(req.user.id, req.params.index)])
		.then(() => res.sendStatus(200))
		.catch(err => {
			console.log(err);
			res.status(500).send("cannot delete");
		});
});


router.post(`/accounts/:index/transactions/new`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	Account.getById(req.params.index).populate('transactions').exec()
		.then(account => {
			if (account) {
				const kind = req.body.kind;
				const category = req.body.category;
				const count = parseInt(req.body.count);
				const currency = req.body.currency;
				const imgUrl = Transaction.checkCategory(category);
				let transaction = new Transaction(account._id, kind, category, currency, count, imgUrl, req.user.id);
				return Transaction.insert(transaction);
			}
			else {
				res.status(404).json({
					message: `there are no accounts with id - ${req.params.index}`
				});
			}

		})
		.then(transaction => {
			return Promise.all([Account.addTransaction(transaction), transaction]);
		})
		.then(([account, transaction]) => {
			res.status(201).json(transaction);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send(`Cannot add transaction to account with id - ${req.params.index}`);
		});

});


router.get(`/transactions`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	Transaction.getAllFromUser(req.user.id)
		.then(transactions => {
			const page = req.query.page || 0;
			const limit = 3;
			const offset = page * limit;
			if (req.query.str) {
				const category = req.query.str;
				Transaction.getByCategory(category).populate('account').exec()
					.then(transactions => {
						if (transactions) {
							let data = Transaction.pagination(transactions.length, limit, offset, parseInt(page), transactions);
							data.user = req.user;
							res.status(200).json(data.transactions);
						}
					})
					.catch(err => {
						console.log(err);
						res.status(500).send("cannot get category in search");
					});
			}
			else {
				if (transactions) {
					let data = Transaction.pagination(transactions.length, limit, offset, parseInt(page), transactions);
					data.user = req.user;
					res.status(200).json(data.transactions);
				}
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems...");
		});
});


router.get(`/transactions/:index`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	let id = req.params.index;
	Transaction.getById(id)
		.populate('account')
		.exec()
		.then(transaction => {
			if (typeof transaction === "undefined") {
				res.status(404).json({
					message: `there are no transactions with id - ${req.params.index}`
				});
			} else {
				let data = {
					user: req.user,
					transaction: transaction
				};
				res.status(200).json(data.transaction);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems...");
		});
});

router.patch(`/transactions/:index/update`, passport.authenticate(`basic`, { session: false }), (req, res) => {
	Transaction.getById(req.params.index).populate('account').exec()
		.then(transaction => {
			if (typeof transaction === "undefined") {
				res.status(404).json({
					message: `there are no transactions with id - ${req.params.index}`
				});
			}
			else {
				const kind = (req.body.kind === null) ? transaction.kind : req.body.kind;
				const category = (req.body.category === null) ? transaction.category : req.body.category;
				const count = (req.body.count === null) ? transaction.count : parseInt(req.body.count);
				const currency = (req.body.currency === null) ? transaction.currency : req.body.currency;
				let imgUrl = Transaction.checkCategory(category);
				let transactionNew = new Transaction(transaction.account, kind, category, currency, count, imgUrl, req.user.id);
				return Promise.all([Transaction.update(transactionNew, req.params.index), Transaction.getById(transaction._id)]);
			}
		})
		.then(([oldTransaction, newTransaction]) => {
			res.status(200).json(newTransaction);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Something wrong in transaction.js line 129");
		});
});


router.delete(`/transactions/:index`,  passport.authenticate(`basic`, { session: false }), (req, res) => {
	Transaction.getById(req.params.index)
		.then(transaction => {
			return Transaction.delete(transaction._id);
		})
		.then((transaction) => {
			return Account.removeTransaction(transaction.account, transaction);

		})
		.then(() => {
			res.status(200).json({});
		})
		.catch(err => {
			console.log(err);
			res.status(500).send(`Cannot delete transactions with id - ${req.params.index}`);
		});
});
module.exports = router;