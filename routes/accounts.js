const express = require('express');
const router = express.Router();
const Account = require('../models/account');
const User = require('../models/users');
const Transaction = require('../models/transactions');
const common = require('./common');
const session_config = require('../models/session_config');

router.use(express.static('public'));



router.get('/', session_config.checkAuth, (req, res) => { // show all accounts
	User.getById(req.user.id)
		.then(user => {
			if (req.query.str) {
				const name = req.query.str;
				return Account.getByName(name, user.id).populate('transactions').exec();
			}
			else {
				return Account.getAllFromUser(user.id).populate('transactions').exec();
			}
		})
		.then(accounts => {
			const page = req.query.page || 0;
			const limit = 5;
			const offset = page * limit;
			let pagination = Account.pagination(accounts.length, limit, offset, parseInt(page), accounts);
			if (accounts) {
				let data = {
					user: req.user,
					accounts: pagination.accounts,
					pages: pagination.pages,
					pgNum: parseInt(page),
					prevDisabled: pagination.prevDisabled,
					nextDisabled: pagination.nextDisabled,
					prev: pagination.prev,
					next: pagination.next,
					pagesCount: pagination.pagesCount
				};
				res.render('accounts', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Something wrong in /accounts");
		});
});

router.get(`/new`, session_config.checkAuth, (req, res) => { // redirect to page of account creating
	res.render(`newAccount`);
});
router.post('/new', session_config.checkAuth, (req, res) => {
	User.getByLogin(req.user.username)
		.then(user => {
			const photo = req.files.photoFile;
			return Promise.all([common.savePic(photo), user]);

		})
		.then(([photo, user]) => {
			const name = req.body.name;
			const count = parseInt(req.body.count);
			const currency = req.body.currency;
			let account = new Account(name, count, currency, user._id, photo.url);
			return Account.insert(account);
		})
		.then((account) => {
			return User.addAccount(account);
		})
		.then(() => {
			res.redirect(`/accounts`);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Something wrong in /accounts/new (post)");
		});
});




router.get(`/:index/transactions`, session_config.checkAuth, (req, res) => {
	Account.getById(req.params.index).populate('transactions').exec()
		.then(account => {
			const page = req.query.page || 0;
			const limit = 3;
			const offset = page * limit;
			let data = Transaction.pagination(account.transactions.length, limit, offset, page, account.transactions);
			data.user = req.user;
			res.render(`transactions`, data);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("Something wrong in /accounts/:index/transactions (get)");
		});
});


router.get(`/:index/new`, session_config.checkAuth, (req, res) => {
	Account.getById(req.params.index)
		.then(account => {
			if (typeof account === "undefined") res.status(404).send(`There are no accounts with id - ${req.params.index}`);
			else {
				let data = {
					user: req.user,
					account: account
				};
				res.render('newTransaction', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("something wrong in accounts.js");
		});
});

router.post(`/:index/new`, session_config.checkAuth, (req, res) => {
	Account.getById(req.params.index).populate('transactions').exec()
		.then(account => {
			if (account) {
				const kind = req.body.kind;
				const category = req.body.category;
				const count = parseInt(req.body.count);
				const currency = account.currency;
				const imgUrl = Transaction.checkCategory(category);
				let transaction = new Transaction(account._id, kind, category, currency, count, imgUrl, req.user.id);
				return Transaction.insert(transaction);
			}
			else {
				res.status(404).send("cannot find ");
			}

		})
		.then(transaction => {
			return Account.addTransaction(transaction);
		})
		.then(() => {
			res.redirect(`/transactions`);
		})
		.catch(err => {
			console.log(err);
			res.status(500).send(`Cannot add transaction to account with id - ${req.params.index}`);
		});
});



router.get(`/:index/update`, session_config.checkAuth, (req, res) => { //redirect to updating account page 
	Account.getById(req.params.index)
		.populate('transactions')
		.exec()
		.then(account => {
			if (typeof account === "undefined") res.status(404).send(`There are no accounts with id - ${req.params.index}`);
			else {
				let data = {
					user: req.user,
					account: account
				};
				res.render('updateAccount', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("something wrong in accounts/:index/update (get)");
		});
});


router.post(`/:index/update`, session_config.checkAuth, (req, res) => { // update accouunt with index
	Account.getById(req.params.index).populate('transactions').exec()
		.then(account => {
			if (typeof account === "undefined") {
				res.status(404).send(`There is no account with this if - ${req.params.index}`);
				return Promise.reject();
			}
			else {
				const files = req.files.photoFile;
				return Promise.all([common.savePic(files), account]);
			}
		})
		.then(([photo, account]) => {
			const name = req.body.name;
			const count = parseInt(req.body.count);
			const currency = req.body.currency;
			for(let transaction of account.transactions) {
				transaction.currency = account.currency;
			}
			let accountNew = new Account(name, count, currency, account.owner, photo.url);
			return Account.update(accountNew, req.params.index);
		})
		.then((account) => {
			return Account.getById(account.id);
		})
		.then(account => {
			return Transaction.updateAllFromAccount(account);
		})
		.then(() => res.redirect(`/accounts/${req.params.index}`))
		.catch(err => {
			console.log(err);
			res.status(500).send("Cannot update this account in line 109");
		});

});

router.get('/:index', session_config.checkAuth, (req, res) => { // get account with id
	let id = req.params.index;
	Account.getById(id)
		.then(account => {
			if (typeof account === "undefined") {
				res.status(404).send("cannot find account");
			} else {
				let data = {
					user: req.user,
					account: account
				};
				res.render('account', data);
			}
		})
		.catch(err => {
			console.log(err);
			res.status(500).send("There are some problems...");
		});
});

router.post('/:index', session_config.checkAuth, (req, res) => {
	Promise.all([Transaction.deleteFromAccount(req.params.index), Account.delete(req.params.index), User.removeAccount(req.user._id, req.params.index)])
		.then(() => res.redirect(`/accounts`))
		.catch(err => {
			console.log(err);
			res.status(500).send("cannot delete");
		});
});




























// *****************************************************************************************
// router.get(`/:index/update`, (req, res) => { //redirect to updating account page 
// 	Accounts.getById(req.params.index)
// 		.populate('transactions')
// 		.exec()
// 		.then(account => {
// 			if (typeof account === "undefined") res.status(404).send(`There are no accounts with id - ${req.params.index}`)
// 			else {
// 				res.render('accUpdate', account);
// 			}
// 		})
// 		.catch(err => {
// 			console.log(err);
// 			res.status(500).send("something wrong in accounts.js line 59");
// 		});
// });

// router.post(`/:index/update`, (req, res) => { // update accouunt with index
// 	Accounts.getById(req.params.index)
// 		.then(account => {
// 			if (typeof account === "undefined") {
// 				res.status(404).send(`There is no account with this if - ${req.params.index}`);
// 			}
// 			else {
// 				const name = (!req.body.name === null) ? account.name : req.body.name;
// 				const count = (!req.body.count) ? account.count : parseInt(req.body.count);
// 				const currency = (!req.body.currency === null) ? account.currency : req.body.currency;
// 				const files = req.files.photoFile;
// 				if (files) {
// 					common.savePic(files)
// 						.then((photo) => {
// 							let accountNew = new Accounts(name, count, currency, account.owner, photo.url);
// 							Accounts.update(accountNew, req.params.index)
// 								.then(() => res.redirect(`/accounts/${req.params.index}`))
// 								.catch(err => {
// 									console.log(err);
// 									res.status(500).send("Cannot update this account in line 109");
// 								});
// 						})
// 						.catch(err => {
// 							console.log(err);
// 							res.status(500).send("Cannot save this photo");
// 						});
// 				}
// 				else {
// 					let accountNew = new Accounts(name, count, currency, account.owner, account.imgUrl);
// 					console.log(count);
// 					Accounts.update(accountNew, req.params.index)
// 						.then(() => res.redirect(`/accounts/${req.params.index}`))
// 						.catch(err => {
// 							console.log(err);
// 							res.status(500).send("Cannot update this account in line 123");
// 						});
// 				}
// 			}
// 		})
// 		.catch(err => {
// 			console.log(err);
// 			res.status(500).send("Something wrong in accounts.js line 129");
// 		});
// });
// router.post(`/:index`, (req, res) => { // delete account
// 	Transactions.deleteFromAccount(req.params.index)
// 		.then(() => Accounts.delete(req.params.index))
// 		.then(() => res.redirect(`/accounts`))
// 		.catch(err => {
// 			console.log(err);
// 			res.status(500).send("cannot delete")
// 		});
// });

// router.get(`/:index/new`, (req, res) => { // redirects to ccreating transaction page
// 	Accounts.getById(req.params.index)
// 		.then(account => {
// 			if (typeof account === "undefined") res.status(404).send(`There are no accounts with id - ${req.params.index}`)
// 			else {
// 				res.render('transNew', account);
// 			}
// 		})
// 		.catch(err => {
// 			console.log(err);
// 			res.status(500).send("something wrong in accounts.js");
// 		});
// });

// router.post('/:index/new', (req, res) => { // creates transaction
// 	Accounts.getById(req.params.index).populate('transactions').exec()
// 		.then(account => {
// 			const kind = req.body.kind;
// 			const category = req.body.category;
// 			const count = parseInt(req.body.count);
// 			const currency = req.body.currency;
// 			const imgUrl = Transactions.checkCategory(category);
// 			let transaction = new Transactions(account._id, kind, category, currency, count, imgUrl);
// 			return Transactions.insert(transaction);
// 		})
// 		.then(transaction => {
// 			return Accounts.addTransaction(transaction);
// 		})
// 		.then(() => {
// 			res.redirect(`/accounts/${req.params.index}`);
// 		})
// 		.catch(err => {
// 			console.log(err);
// 			res.status(500).send(`Cannot add transaction to account with id - ${req.params.index}`);
// 		});

// })










module.exports = router;