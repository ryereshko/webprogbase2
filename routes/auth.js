const express = require('express');
const User = require('../models/users.js');
const router = express.Router();
const config = require('../config.js');
const session_config = require('../models/session_config');
const common = require('../routes/common');
const cloudinary = require('cloudinary');

router.use(express.static('public'));


cloudinary.config({
	cloud_name: config.cloudinary.cloud_name,
	api_key: config.cloudinary.api_key,
	api_secret: config.cloudinary.api_secret
});

const passport = require('../models/passport_config.js');






router.get('/register', (req, res) => {
	res.render('register');
});
router.post(`/register`, (req, res, next) => {
	let username = req.body.username;
	const files = req.files.image;
	User.getByLogin(username)
		.then(user => {
			if (!user) {
				const password = req.body.password;
				const confirm = req.body.confirm;
				if (password !== confirm) {
					res.redirect('/auth/register?error=Passwords+do+not+match');
				}
				else {
					const username = req.body.username;
					const fullname = req.body.fullname;
					common.savePic(files)
						.then(photo => {
							let user = new User(username, session_config.sha512(password, session_config.serverSalt).passwordHash, fullname, photo.url);
							return User.insert(user);
						})
						.then(() => {
							next();
						})
						.catch(err => {
							console.log(err);
							res.status(500).send(err.message());
						});
				}
			} else {
				res.redirect('/auth/register?error=Username+already+exists');

			}
		})
		.catch((err) => {
			console.log(err);
			res.status(500).send("Problems in auth");
		});

},
	passport.authenticate('local', {
		successRedirect: '/user',
		failureRedirect: '/auth/login'
		//failureFlash: true
	}));

router.get('/login', function (req, res) {
	//TODO: render login page
	console.log(`User connected`);
	res.render('login', { user: req.user });
});

router.post('/login', passport.authenticate('local', {
	successRedirect: '/user',
	failureRedirect: '/auth/login'
	//failureFlash: true
}));

router.get('/logout', session_config.checkAuth, function (req, res) {
	//TODO: logout user
	req.logout();
	console.log('user logged out');
	res.redirect('/');
});




module.exports = router;