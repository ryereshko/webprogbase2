const config = require('../config');
const cloudinary = require('cloudinary');
cloudinary.config({
	cloud_name: config.cloudinary.cloud_name,
	api_key: config.cloudinary.api_key,
	api_secret: config.cloudinary.api_secret
});

module.exports = {
	savePic: function (file) {
		return new Promise((resolve, reject) => {
			const fileObject = file;
			const fileBuffer = fileObject.data;
			cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
				function (error, result) {
					if(error) reject(error);
					else {
						resolve(result);
					}
				})
				.end(fileBuffer);
		});
	},
};