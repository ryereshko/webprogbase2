const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const AccountSchema = new mongoose.Schema({
	name: { type: String, required: true },
	owner: { type: Schema.Types.ObjectId, ref: "User" },
	createdAt: { type: Date, default: Date.now },
	count: { type: Number, required: true },
	currency: { type: String, required: true },
	transactions: [{
		type: Schema.Types.ObjectId,
		ref: 'Transaction'
	}],
	imgUrl: { type: String, required: true }
});
const AccountModel = mongoose.model("Account", AccountSchema);



class Account {
	constructor(name, count, currency, owner, imgUrl) {
		this.name = name;
		this.count = count;
		this.currency = currency;
		this.owner = owner;
		this.imgUrl = imgUrl;
	}
	static getAll(limit, skip) {
		return AccountModel.find().limit(limit).skip(skip);
	}
	static getAllFromUser(userId) {
		return AccountModel.find({owner: userId});
	}
	static getByIdFromUser(accountId, userId) {
		return AccountModel.findOne({_id: accountId, owner: userId});
	}
	static getById(id) {
		return AccountModel.findOne({ _id: id });
	}
	static getByName(name, userId) {
		return AccountModel.find({
			name: new RegExp(name, `i`),
			owner: userId
		});
	}
	static insert(x) {
		return new AccountModel(x).save();
	}
	static delete(id) {
		return AccountModel.findOneAndRemove({ _id: id });
	}
	static update(x, id) {
		return AccountModel.findByIdAndUpdate({ _id: id }, {
			$set: {
				name: x.name,
				count: x.count,
				currency: x.currency,
				owner: x.owner,
				imgUrl: x.imgUrl,
			}
		}).populate('transactions');
	}
	static addTransaction(transaction) {
		return AccountModel.findOneAndUpdate({ _id: transaction.account }, {
			$push: { transactions: transaction._id },
			$inc: { count: transaction.count }
		});
	}
	static removeTransaction(accountId, transaction) {
		return AccountModel.findOneAndUpdate({ _id: accountId }, {
			$pull: { transactions: transaction._id },
			$inc: { count: -transaction.count }
		});
	}
	static deleteFromUser(userId) {
		return AccountModel.deleteMany({ owner: userId });
	}
	static updateCount(id, count) {
		return AccountModel.findOneAndUpdate({ _id: id }, {
			$set: {
				count: count
			}
		});
	}
	static pagination(length, limit, position, currentPage, accounts) {
		const count = Math.ceil(length / limit);
		let pages = new Array();
		let next, prev, nextDisabled, prevDisabled;
		let OffSetAccounts = accounts.splice(position, limit);
		for (let i = 0; i < count; i++) {
			pages[i] = {};
			if (i === currentPage) pages[i].isActive = "active";
			else {
				pages[i].isActive = "";
			}
			pages[i].pgNum = i;
		}
		if (currentPage === 0) {
			next = currentPage + 1;
			prev = currentPage;
			nextDisabled = "";
			prevDisabled = "disabled";
			if (pages.length === 1 || pages.length === 0) {
				nextDisabled = "disabled";
			}
		}
		else {
			if (currentPage === count - 1) {
				next = currentPage;
				prev = currentPage - 1;
				nextDisabled = "disabled";
				prevDisabled = "";
			}
			else {
				next = currentPage + 1;
				prev = currentPage - 1;
				nextDisabled = "";
				prevDisabled = "";
			}
		}
		return {
			accounts: OffSetAccounts,
			pages: pages,
			next: next,
			prev: prev,
			nextDisabled: nextDisabled,
			prevDisabled: prevDisabled,
			pagesCount: pages.length
		};
	};

}

module.exports = Account;