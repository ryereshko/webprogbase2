
const mongoose = require('mongoose');




const TransactionSchema = new mongoose.Schema({
	account: { type: mongoose.mongo.ObjectId, ref: 'Account' },
	kind: { type: String, required: true },
	category: { type: String, required: true },
	currency: { type: String, required: true },
	count: { type: Number, required: true },
	createdAt: { type: Date, default: Date.now },
	imgUrl: { type: String, required: true },
	owner: { type: mongoose.mongo.ObjectId, required: true}
});
const TransactionModel = mongoose.model('Transaction', TransactionSchema);


class Transaction {
	constructor(account, kind, category, currency, count, imgUrl, owner) {
		this.account = account;
		this.kind = kind;
		this.category = category;
		this.currency = currency;
		this.count = (kind === 'fee') ? -count: count;
		this.imgUrl = imgUrl;
		this.owner = owner;
	}
	static getAll() {
		return TransactionModel.find();
	}
	static getAllFromUser(userId) {
		return TransactionModel.find({owner: userId});
	}
	static getById(id) {
		return TransactionModel.findOne({ _id: id });
	}
	static getByCategory(category)	{
		return TransactionModel.find({
			category: new RegExp(category, `i`)
		});
	}
	static insert(x) {
		return new TransactionModel(x).save();
	}
	static delete(id) {
		return TransactionModel.findOneAndRemove({ _id: id });
	}
	static update(x, id) {
		return TransactionModel.findByIdAndUpdate({_id: id}, { $set:{
			kind: x.kind,
			count: x.count,
			currency: x.currency,
			account: x.account,
			imgUrl: x.imgUrl,
			category: x.category,
			owner: x.owner
		}}).populate('account');
	}
	static updateAllFromAccount(account) {
		return TransactionModel.updateMany({account: account.id}, { $set: {
			currency: account.currency,
		}}).populate('account');
	}
	static deleteFromAccount(accountId) {
		return TransactionModel.deleteMany({ account: accountId });
	}
	static checkCategory(category) {
		let imgUrl = "";
		switch (category) {
			case "transport":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/transport.jpg";
				break;
			case "cafes&restaurants":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/restoran.png";
				break;
			case "groceries":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/groceries.jpg";
				break;
			case "clothing":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664322/budgControl/transactions/clothing.png";
				break;
			case "house":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/house.png";
				break;
			case "entertainment":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664322/budgControl/transactions/entertainment.png";
				break;
			case "vacation":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/vacation.png";
				break;
			case "health":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664323/budgControl/transactions/health.png";
				break;
			case "gadgets":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664322/budgControl/transactions/gadgets.png";
				break;
			case "cell":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664322/budgControl/transactions/cell.png";
				break;
			case "no category":
				imgUrl = "https://res.cloudinary.com/dzxphimuo/image/upload/v1542664324/budgControl/transactions/without.png";
				break;
		}
		return imgUrl;
	}


	static pagination(length, limit, position, currentPage, accounts) {
		const count = Math.ceil(length / limit);
		let pages = new Array();
		let next, prev, nextDisabled, prevDisabled;
		let OffSetTransactions = accounts.splice(position, limit);
		for (let i = 0; i < count; i++) {
			pages[i] = {};
			if (i === currentPage) pages[i].isActive = "active";
			else {
				pages[i].isActive = "";
			}
			pages[i].pgNum = i;
		}
		if (currentPage === 0) {
			next = currentPage + 1;
			prev = currentPage;
			nextDisabled = "";
			prevDisabled = "disabled";
			if (pages.length === 1 || pages.length === 0) {
				nextDisabled = "disabled";
			}
		}
		else {
			if (currentPage === count - 1) {
				next = currentPage;
				prev = currentPage - 1;
				nextDisabled = "disabled";
				prevDisabled = "";
			}
			else {
				next = currentPage + 1;
				prev = currentPage - 1;
				nextDisabled = "";
				prevDisabled = "";
			}
		}
		return {
			transactions: OffSetTransactions,
			pages: pages,
			next: next,
			prev: prev,
			nextDisabled: nextDisabled,
			prevDisabled: prevDisabled,
			pagesCount: pages.length
		};
	};
}

module.exports = Transaction;