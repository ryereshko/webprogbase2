const User = require('../models/users');



const LocalStrategy = require('passport-local').Strategy;
const BasicStrategy = require('passport-http').BasicStrategy;

const passport = require('passport');

const support = require('../models/session_config');


passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
	User.getById(id).populate('accounts').exec()
		.then(user => {
			done(null, user);
		})
		.catch(err => {
			console.log(err + " in passport_config line 22");
			done(err, null);
		});
});
passport.use(new BasicStrategy(
	function(username, password, done) {
		let hash = support.sha512(password, support.serverSalt).passwordHash;
		User.getByLoginAndPassword(username, hash).populate('accounts').exec()
			.then(user => {
				done(null, user);
			})
			.catch(err => {
				console.log(err + " with basicStrategy");
				done(err, null);
			}); 
	}
));
passport.use(new LocalStrategy(
	function(username, password, done){
		let hash = support.sha512(password, support.serverSalt).passwordHash;
		User.getByLoginAndPassword(username, hash).populate('accounts').exec()
			.then(user => {
				done(null, user);
			})
			.catch(err => {
				console.log(err + " in passport._config line 36");
				done(err, null);
			});
	}));

module.exports = passport;