

// set User schema and creat model
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new Schema({
	username: { type: String, required: true },
	password: {type: String, required: true},
	fullname: { type: String },
	createdAt: { type: Date, default: Date.now },
	role: { type: Number, default: 0},
	isDisabled: { type: Boolean, default: false },
	imgUrl: {type: String, required: true},
	accounts: [{
		type: Schema.Types.ObjectId,
		ref: 'Account'
	}],
});
const UserModel = mongoose.model("User", UserSchema);


class User {
	constructor(username, password, fullname, imgUrl) {
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.imgUrl = imgUrl;
	}
	static getAll() {
		return UserModel.find();
	}
	static getById(id) {
		return UserModel.findOne({_id: id});
	}
	static getByLogin(username) {
		return UserModel.findOne({username: username});
	}
	static getByLoginAndPassword(username, password) {
		return UserModel.findOne({
			username: username,
			password: password
		});
	}
	static insert(x) {
		return new UserModel(x).save();
	}
	static delete(x) {
		return UserModel.findOneAndDelete({_id: x._id});
	}
	static addAccount(account) {
		return UserModel.findByIdAndUpdate({_id: account.owner}, { $push: {accounts: account._id}});
	}
	static removeAccount(userId, accountId) {
		return UserModel.findByIdAndUpdate({_id: userId}, { $pull: {accounts: accountId}});
	}
	static updateRole(userId, role) {
		return UserModel.findByIdAndUpdate({_id: userId}, {$set: {role: role}});
	}
	static pagination(length, limit, position, currentPage, users) {
		const count = Math.ceil(length / limit);
		let pages = new Array();
		let next, prev, nextDisabled, prevDisabled;
		let OffSetUsers = users.splice(position, limit);
		for (let i = 0; i < count; i++) {
			pages[i] = {};
			if (i === currentPage) pages[i].isActive = "active";
			else {
				pages[i].isActive = "";
			}
			pages[i].pgNum = i;
		}
		if (currentPage === 0) {
			next = currentPage + 1;
			prev = currentPage;
			nextDisabled = "";
			prevDisabled = "disabled";
			if (pages.length === 1 || pages.length === 0) {
				nextDisabled = "disabled";
			}
		}
		else {
			if (currentPage === count - 1) {
				next = currentPage;
				prev = currentPage - 1;
				nextDisabled = "disabled";
				prevDisabled = "";
			}
			else {
				next = currentPage + 1;
				prev = currentPage - 1;
				nextDisabled = "";
				prevDisabled = "";
			}
		}
		return {
			users: OffSetUsers,
			pages: pages,
			next: next,
			prev: prev,
			nextDisabled: nextDisabled,
			prevDisabled: prevDisabled,
			pagesCount: pages.length
		};
	};
}




module.exports = User;